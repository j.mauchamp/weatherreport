import React, { useState, useEffect } from 'react';
import { Platform, Image, Text, View, StyleSheet, FlatList, Alert} from 'react-native';
import * as Location from 'expo-location';

export default function App() {
  const [localisation, setLocalisation] = useState(null);
  const [errorMsg, setErrorMsg] = useState(null);
  const [name, setName] = useState('Chargement..');
  const [temp, setTemp] = useState(null);
  const [description, setDescription] = useState(null);
  const [icon, setIcon] = useState(null);
  const [forecast, setForecast] = useState(null);

  const getLocalisation = (latitude, longitude) => {
    return fetch('https://api.openweathermap.org/data/2.5/weather?lat='+ latitude +'&lon='+ longitude +'&exclude=daily&appid=6c18a8c7bd69137ca66249d56f141b14&lang=fr&units=metric')
        .then((response) => response.json())
        .then((json) => {
          setName(json.name)
          setTemp('Température: ' + json.main.temp+'°C')
          setDescription('déscription: ' + json.weather[0].description)
          setIcon('http://openweathermap.org/img/wn/' + json.weather[0].icon + '.png')
          return json.name
        })
        .catch((error) => {
          console.error(error);
        });
  };

  const getForecast = (lat, long) => {
      fetch('https://api.openweathermap.org/data/2.5/onecall?lat='+lat+'&lon='+long+'&appid=db0aa59a06b91c3d0b88cc186720d92c&units=metric&lang=fr&&exclude=hourly,alert,minutely')
          .then(response => response.json())
          .then(data => {
            setForecast(data.daily)
            console.log("data: "+data)
          })
  }

  useEffect(() => {
    (async () => {
      let { status } = await Location.requestForegroundPermissionsAsync();
      if (status !== 'granted') {
        setErrorMsg('Permission to access location was denied');
        return;
      }

      let location = await Location.getCurrentPositionAsync({});
      setLocalisation(location);
      getLocalisation(location.coords.latitude, location.coords.longitude);
      getForecast(location.coords.latitude, location.coords.longitude);
    })();
  }, []);

  const timeConverter = (dt) =>{
    var a = new Date(dt * 1000);
    var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    var year = a.getFullYear();
    var month = months[a.getMonth()];
    var date = a.getDate();
    var hour = a.getHours();
    var min = a.getMinutes();
    var sec = a.getSeconds();
    var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
    return time;
  }
  console.log(timeConverter(0));

  const Item = ({ title }) => (
    <View style={styles.item}>
        <Text style={styles.title}>{timeConverter(title.dt)}</Text>
        <Text style={styles.title}>{title.weather[0].description}</Text>
        <Text style={styles.title}>{title.temp.day}°C</Text>
        <Image
        style={styles.tinyLogo}
        source={{uri: 'http://openweathermap.org/img/wn/' + title.weather[0].icon + '.png'}}
      />
    </View>
  );

  const renderItem = ({ item }) => (
      <Item title={item} />
  );

  return (
    <View style={styles.container}>
      <Text style={styles.paragraph}>{name}</Text>
      <Text style={styles.paragraph}>{temp}</Text>
      <Text style={styles.paragraph}>{description}</Text>
      <Image
        style={styles.tinyLogo}
        source={{uri: icon}}
      />
      <FlatList data={forecast} renderItem={renderItem}/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 40,
    backgroundColor: "gray"
  },
  paragraph: {
    fontSize: 18,
    textAlign: 'center',
  },
  tinyLogo: {
    width: 50,
    height: 50,
  },
  item: {
    backgroundColor: '#6F7484',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
    borderRadius: 10
},

});